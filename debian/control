Source: qcengine
Section: python
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Build-Depends: debhelper (>= 12),
               dh-python,
               python3-all,
               python3-cpuinfo,
               python3-msgpack,
               python3-numpy,
               python3-setuptools,
               python3-pint,
               python3-psutil,
               python3-pydantic (>= 1.8.2),
               python3-pytest,
               python3-pytest-cov,
               python3-qcelemental (>= 0.23.0)
Standards-Version: 4.1.3
Homepage: https://github.com/MolSSI/QCEngine
Vcs-Browser: https://salsa.debian.org/debichem-team/qcengine
Vcs-Git: https://salsa.debian.org/debichem-team/qcengine.git

Package: python3-qcengine
Section: python
Architecture: all
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: Quantum chemistry program executor and IO standardizer
 QCEngine provides a wrapper to ingest and produce QCSchema for a variety of
 quantum chemistry programs, most notably Psi4, NWchem, RDKit and Mopac.
 .
 Other supported (possibly proprietary) packages include DFTD3, MOLPRO, MPD2,
 OpenMM, Q-Chem, Turbomole, xtb, CFour, GAMESS, Terachem and TorchANI.
